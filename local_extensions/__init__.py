from jinja2.ext import Extension, pass_context


@pass_context
def get_doc_url(context, prefix="https://"):
    dough_vars = context.parent["cookiecutter"]
    remote_provider = dough_vars["remote_provider"]
    if remote_provider == "none":
        return "none"
    
    if remote_provider == "gitlab.com":
        url = "{remote_namespace}.gitlab.io/{project_slug}"
    elif remote_provider == "github.com":
        url = "{remote_namespace}.github.io/{project_slug}"
    elif remote_provider == "gitlab.mpcdf.mpg.de":
        url = "{remote_namespace}.pages.mpcdf.de/{project_slug}"
    elif remote_provider == "git.ipp-hgw.mpg.de":
        url = "{remote_namespace}.gitlab-pages.ipp-hgw.mpg.de/{project_slug}"
    else:
        url = "{remote_namespace}.{remote_namespace.replace('.com', '.io')}/{project_slug}"
    
    url = prefix + url
    url = url.format(**dough_vars)
    return url


@pass_context
def get_remote_namespace_url(context, prefix="https://"):
    dough_vars = context.parent["cookiecutter"]
    remote_provider = dough_vars["remote_provider"]
    if remote_provider == "none":
        return "none"
    if prefix == "git@":
        url = "{remote_provider}:{remote_namespace}"    
    else:
        url = "{remote_provider}/{remote_namespace}"
    url = prefix + url
    url = url.format(**dough_vars)
    return url


@pass_context
def get_remote_url(context, prefix="https://"):
    dough_vars = context.parent["cookiecutter"]
    remote_provider = dough_vars["remote_provider"]
    if remote_provider == "none":
        return "none"
    url = get_remote_namespace_url(context, prefix) + "/{project_slug}"
    url = url.format(**dough_vars)
    return url


def omit_extensions(dict_):
    return {
        key: value
        for key, value in dict_.items()
        if key not in ("_extensions", "_output_dir")
    }


class DoughExtension(Extension):
    def __init__(self, environment):
        super().__init__(environment)
        environment.globals.update(
            {
                "get_doc_url": get_doc_url,
                "get_remote_url": get_remote_url,
                "get_remote_namespace_url": get_remote_namespace_url,
            }
        )
        environment.filters["omit_extensions"] = omit_extensions
