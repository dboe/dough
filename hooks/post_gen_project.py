#!/usr/bin/env python
import os
import shutil
import subprocess
import webbrowser
import sys
import logging


# Get the root project directory
PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)

sys.path.append(PROJECT_DIRECTORY)

from tools.process import run_shell_command


def remove_files():
    if '{{ cookiecutter.data_science }}' != "y":
        remove_data_scinece()

    if '{{ cookiecutter.continuous_integration }}' == "y":
        ci_remote_mapping = {
            "gitlab.com": "gitlab",
            "github.com": "travis",
            "gitlab.mpcdf.mpg.de": "gitlab",
            "git.ipp-hgw.mpg.de": "gitlab",
            "None": None
        }
        ci_flavour = ci_remote_mapping['{{ cookiecutter.remote_provider }}']
        keep_ci(ci_flavour)


def remove_data_scinece():
    shutil.rmtree(os.path.join(PROJECT_DIRECTORY, 'data'))
    shutil.rmtree(os.path.join(PROJECT_DIRECTORY, 'models'))
    shutil.rmtree(os.path.join(PROJECT_DIRECTORY, 'notebooks'))
    shutil.rmtree(os.path.join(PROJECT_DIRECTORY, 'references'))
    shutil.rmtree(os.path.join(PROJECT_DIRECTORY, 'reports'))


def keep_ci(flavour):
    if flavour != 'gitlab':
        os.remove(os.path.join(PROJECT_DIRECTORY, '.gitlab-ci.yml'))
    if flavour != 'travis':
        os.remove(os.path.join(PROJECT_DIRECTORY, '.travis.yml'))


def process_git_init():
    if not os.path.exists(os.path.join(PROJECT_DIRECTORY, '.git')):
        print("calling 'git init'")
        subprocess.call(['git', 'init'])
        print("calling 'git add *'")
        subprocess.call(['git', 'add', '*'])
        print("calling 'git commit -m ...'")
        subprocess.call(['git', 'commit', '-m', 'chore(git): initial commit'])
        

def process_git_remote():
    {%- if cookiecutter.remote_provider != "none" %}
    no_remote = False
    try:
        run_shell_command("git ls-remote --exit-code")
    except subprocess.CalledProcessError as err:
        if err.returncode != 128:
            raise err
        no_remote = True
    if no_remote:
        url = "{{ get_remote_namespace_url() }}"
        webbrowser.open(url)
        input("\n✫ Your chosen remote git-provider just opened, please create a blank "
              "project with the name '{{ cookiecutter.package_name }}' "
              " and the project slug '{{ cookiecutter.project_slug }}'."
              " You could add the description '{{ cookiecutter.summary }}'."
              " If you are done, press enter to continue. ")
        full_url = "{{ get_remote_url(prefix='git@') }}" + '.git'
        print("\ncalling 'git remote add origin {full_url}'".format(**locals()))
        subprocess.call(['git', 'remote', 'add', 'origin', full_url])
        print("\n✫ Please call 'git push -u origin master' if you want to push after this setup.")
    {%- else %}
    pass
    {%- endif %}


def process_ci():
    if '{{ cookiecutter.continuous_integration }}' == "y":
        print(
            "\n✫ To set up continuous integration, follow the instructions at "
            "https://gitlab.com/dboe/dough#ci-variables"
        )
    else:
        pass


def create_env():
    # call poetry install to build locks. pixi shell would kill this process
    run_shell_command("pixi install")  # this does not trigger setup.sh
    run_shell_command("pixi run setup")
    run_shell_command("pixi run lock")
    run_shell_command("git add *.lock")
    run_shell_command("git commit -m 'build(dependencies): adding lock files' --no-verify")
    print("")  # \n does not work in front of cookie :)
    print("🍪 Done")
    # this should be the last thing you do.
    run_shell_command("pixi shell")
       

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    remove_files()
    if not "cookiecutter" in PROJECT_DIRECTORY:
        # Don't do this when calling 'pixi run upgrade' triggering cookiecutter_project_upgrader
        process_git_init()
        process_ci()
        process_git_remote()
        create_env()
