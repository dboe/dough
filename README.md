# Dough 🍪

Dough is a [cookiecutter](https://cookiecutter.readthedocs.io/en/latest/) **template** aiming to implement software-life-cycle work-flows with **template inheritance** (automatically *rebase* your project to the latest dough template) with **dialect consitency**.

#  Example packages created with dough:

To get an idea of the potency of this template, have a look at one of these examples' infrastructure (ignore project-specific content)

* https://gitlab.mpcdf.mpg.de/dboe/rna
* https://gitlab.mpcdf.mpg.de/dboe/tfields
* https://gitlab.mpcdf.mpg.de/dboe/w7x

All these projects are closely related.
Bugs concerning the template they are jointly built with are fixed once and then rolled out to all other via

```shell
pixi run upgrade
```

That's what is meant by **template inheritance**.

# Prerequisites

[Install cookiecutter via pixi or pip](https://cookiecutter.readthedocs.io/en/latest/installation.html).

<details>
<summary>Pixi</summary>

Either you directly install cookiecutter from the cookiecutter conda-forge channel via

```shell
pixi global install cookiecutter --expose cookiecutter
```

or you install it inside a python environment via pip


```shell
pixi global install python==3.10 --with pip --expose pip --expose python
pip install cookiecutter
```

In case the binary is not found after these commands ran succesfully, probably you did not add the global binaries under `$HOME/.pixi/bin` to your PATH environment variable.
Under linux you would add

```shell
export PATH="$HOME/.pixi/bin"
```

to your `.bashrc` to fix that.

</details>

# Setup

To roll out the template for a project, choose a [Dialect](#dialects) and run


``` bash
cookiecutter https://gitlab.com/dboe/dough.git --checkout <dialect>
```

select and fill the prompted [Option Choices](#options) and follow the instructions for further [Settings](#settings).

<details>
<summary>Optional: upgrade existing package</summary>

If you want to upgrade an already existing package with dough, run the above command with the ```-s``` option (skip-if-file-exists) or even the ```-f``` option (overwrite-if-file-exists) i.e.

``` bash
cookiecutter https://gitlab.com/dboe/dough.git -s --checkout <dialect>
```

one directory above the project you want to invade.
Make sure to use the same project name.
</details>

## Dialects

This project intends to provide a similar development procedure for various language dialects (python, cpp, ...).
E.g. `pixi run docs` should build and serve the docs no matter what dialect you are developing in.
This is what is meant by **dialect consistency**.
Dialects correspond to different branches of dough hereafter referred to by \<dialect\>.

Note:
(master=python)

## Options:

In the following we explain the different prompt fields requested by cookiecutter:

- **author**: Name of the main author
- **email**: E-mail of the main author
- **package_name**: name of the package/project. This is the name, used to reference the package in natural language.
- **project_slug**: name of the package/project. This is the name, used for the remote repository and the root folder of your project
- **distribution_name**: Name of the project in distribution. I.e. to import the package in python use import <distribution_name>
- **package_version**: version of the package
- **summary**: 
- **keywords**: comma separated tags, used to categorize the project for pypi and your git remote provider.
- **data_science**: If data science is chosen, you get a few extra directories (like `notebooks` or `data`) to standardize data pipelines paths. Also, `dvc`` is used. DVC requires [further setup](#settings)
- **remote_provider**: Choice of your remote provider (e.g. gitlab)
- **remote_namespace**: Namespace under which you want your project ot be seen (e.g. gitlab.com/<remote_namespace>/<distribution_name>)
- **continuous_integration**: If you want continuous integrations (linting, tests, building, pages, publishing)
- **pypi_username**: Username required to publish to pypi. Use none to disable pypi
- **copyright_license**: Chose a license
- **copyright_holder**: Holder of the license

## Settings

After [roll-out](#usage) you might have to set up some things manually that this project has no possibility to automize:

<details>
<summary><b>Continuous integration (CI) variables</b></summary>

First of all

1) Go to  \<project-url\>/-/settings/ci_cd
2) Expand Variables
3) If pypi upload is wished, create a pypi token for you project
4) If pypi upload is wished, add a variable ``PYPI_USERNAME`` with the value '__token__' (or for ipp.hgw your actual username)
5) If pypi upload is wished, add a variable ``PYPI_PASSWORD`` with the pypi token. Set the password to be masked (or for ipp.hgw your actual password)
</details>

<details>
<summary><b>Data version control (DVC)</b></summary>

If you are new to it, find the [resources of dvc here](https://dvc.org).
We want to set up dvc, so first thing is to [initialize a dvc project](https://dvc.org/doc/start) like so:

```
dvc init
```

This creates config files for dvc that are already added (git add) to git. Commit them:

```
git commit -m "build: Initialize DVC"
```

Now you have to select a DVC remote that your data should be stored at.
There is plenty of options that you can find [here](https://dvc.org/doc/command-reference/remote/add).
We will specify a few additional setup details for a few backends:

<details>
<summary><i>Public</i> Webdav with nextcloud / owncloud</summary>

In your nextcloud/owncloud, find the public webdav url (Settings -> WebDav - replace "https" by "webdavs" and "remote.php/dav/path/to/your/user/" by "public.php/webdav).
Add this remote by calling (replace appropriately)

```
dvc remote add -d datashare webdavs://datashare.mpcdf.mpg.de/public.php/webdav
```

This created your .dvc/config file which you have to add and commit:

```
git add .dvc/config
git commit -m "builj: jadded webdav remote"
```

In the cloud create a folder that you want your dvc files to be shared in.
Create a public link to this file with the Download/View/Edit option and a password (which you should copy/note).
Copy the link to the public share.
Use the share token (last cryptic latter combination at the enfo the the link e.g. "xjJfWlZzjIwreYS") as the user name and optionally the share password (avoid "$" and other shell syntax characters) as the password:

```
dvc remote modify --local datashare user xjJfWlZzjIwreYS
dvc remote modify --local datashare password <your password here>
```

You can modify the above process (e.g. leave the --local flag away, no password, only Download rights, ...) to your needs.

</details>

Further helpful instructions:

<details>
<summary>Clean git repo from large data files</summary>

Remove the file from the index but not locally
```
git rm --cached path/to/my/large/file.npz
```
Add the file to the dvc index and add & commit to git.
```
dvc add path/to/my/large/file.npz
git add path/to/my/large/.gitignore
git add path/to/my/large/file.npz.dvc
git commit -m "refactor: moved large file to data version control"
```
</details>

</details>

# Usage

All usage documentation is included in your project and thus will not be displayed here in copy.
Compile your documentation by running

```shell
pixi run docs
```

If want a quick peek, you can find example of compiled documentation in the [example projects](#example-packages-created-with-dough).

``

# Resources:

## Dialect specific (this branch)
* [Pixi + Poetry] Pixi as environment manager, poetry for publishing functionality (poetry will be removed once pixi takes this functionality over)
