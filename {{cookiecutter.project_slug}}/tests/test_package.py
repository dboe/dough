#!/usr/bin/env python

"""Tests for `{{ cookiecutter.package_name }}` package."""

import unittest

import {{ cookiecutter.distribution_name }}


class TestPackage(unittest.TestCase):
    """Tests for `{{ cookiecutter.package_name }}` package."""

    def setUp(self):
        """Set up test fixtures, if any."""

    def tearDown(self):
        """Tear down test fixtures, if any."""

    def test_version_type(self):
        """Assure that version type is str."""

        self.assertIsInstance({{ cookiecutter.distribution_name }}.__version__, str)
