"""
Check if the working directory is clean
"""

import logging
import subprocess
import sys


LOGGER = logging.getLogger(__name__)


def check_git_status():
    """
    Check if the current working directory is clean, i.e. has no
    uncommitted changes.

    If the directory is not clean, log the Git status and exit
    with code 1.
    """
    git_status = subprocess.check_output(["git", "status", "--porcelain"])
    if git_status:
        LOGGER.error("Your Git status is not clean! Clean up first.")
        LOGGER.info(git_status.decode("utf-8"))
        sys.exit(1)


if __name__ == "__main__":
    check_git_status()
