# pylint: disable=logging-fstring-interpolation
"""
Explicitly set the version. This is a feature missing in poetry_bumpversion.
Since the plan is to update to pixi once pixi replaces all poetry features,
we will not attempt to modify poetry_bumpversion
"""

import logging
import pathlib
import re

from poetry.core.pyproject.toml import PyProjectTOML
from poetry_bumpversion.models import (
    CURRENT_VERSION_MARKER,
    NEW_VERSION_MARKER,
    Instruction,
)
from poetry_bumpversion.plugin import (
    FileSkippedException,
    PluginException,
    read_instructions,
)

LOGGER = logging.getLogger(__name__)


class ExtendedInstruction(Instruction):  # pylint: disable=too-few-public-methods
    """
    Args:
        search_pattern: regex peattern
            (replacing CURRENT_VERSION_MARKER and NEW_VERSION_MARKER with literal version string
        count: passed to re.sub
    """

    re_pattern: str = "__search_phrase__"
    count: int = 0  # substitute all


def update_version_in_file(
    instruction: Instruction,
    current_version: str,
    new_version: str,
    dry_run: bool = False,
) -> None:
    """Process instruction to update the version in file.

    Args:
        instruction: Instruction to process a file.
        current_version (str): The current version in file to be changed.
        new_version (str): The new version to change the current version with.
        dry_run (bool): If True, don't change the file.

    Raises:
        FileSkippedException: when file doesn't exist or doesn't contain the search phase
    """
    if not instruction.file.exists():
        raise FileSkippedException("file not found")

    content = instruction.file.read_text()
    search_phrase = instruction.search_pattern.replace(CURRENT_VERSION_MARKER, current_version)
    replace_phrase = instruction.replace_pattern.replace(NEW_VERSION_MARKER, new_version)
    re_pattern = instruction.re_pattern.replace("__search_phrase__", re.escape(search_phrase))

    if re.search(re_pattern, content) is None:
        raise FileSkippedException(f"file doesn't contain search phrase: {search_phrase}")

    content = re.sub(re_pattern, replace_phrase, content, count=instruction.count)

    if not dry_run:
        instruction.file.write_text(content.replace(search_phrase, replace_phrase))


def bump(new_version, dry_run=False):
    """
    Update the version in files.

    Args:
        new_version (str): The new version
        dry_run (bool): If True, don't change the file.

    Raises:
        PluginException: when no change in version is detected or when nothing to do
    """
    pyproject = PyProjectTOML(pathlib.Path.cwd() / "pyproject.toml")
    current_version = pyproject.data["tool"]["poetry"]["version"]

    if new_version == current_version:
        raise PluginException("no change in version detected")

    # update pyproject itself
    pyproject_instruction = ExtendedInstruction(
        file=pyproject.path,
        search_pattern=f'version = "{CURRENT_VERSION_MARKER}"',
        replace_pattern=f'version = "{NEW_VERSION_MARKER}"',
        # re_pattern = '',
        count=1,
    )
    update_version_in_file(pyproject_instruction, current_version, new_version, dry_run=dry_run)

    instruction_count = 0
    for instruction in read_instructions(pyproject):
        instruction_count += 1
        instruction = ExtendedInstruction(
            file=instruction.file,
            search_pattern=instruction.search_pattern,
            replace_pattern=instruction.replace_pattern,
        )
        try:
            update_version_in_file(instruction, current_version, new_version, dry_run=dry_run)
            LOGGER.info(f"processed file {instruction.file}")
        except FileSkippedException as exc:
            LOGGER.warning(f"skipped file {instruction.file}: {str(exc)}")

    if instruction_count == 0:
        raise PluginException("nothing to do, please add file replacements")


if __name__ == "__main__":
    bump(new_version="test.version.version", dry_run=True)
