function provideScreen() {
    screenName=$1
    program=$2
    running=`screen -list | grep $screenName | wc -l`
    if [ ${running} -lt 1 ]; then
        screen -S "${screenName}" -d -m "${program}" "${@:3}"
        echo "Screen slot ${screenName}: Program ${program} running with options ${@:3}"
    fi
}


if [[ ! -f ".pixi/currently_activating.lock" ]]; then
    # avoid that the setup is run multiple times (tasks will work in pixi shell)
    touch .pixi/currently_activating.lock
    pixi run setup
    rm -f .pixi/currently_activating.lock

    FILE=".pixi/default_editor.txt"
    
    if [[ -f "$FILE" ]]; then
        DEFAULT_EDITOR=$(<"$FILE")
        if command -v "$DEFAULT_EDITOR" &> /dev/null; then
            provideScreen "$DEFAULT_EDITOR" "."
        else
            echo "Error: '$DEFAULT_EDITOR' not found in PATH" >&2
        fi
    fi   

fi
