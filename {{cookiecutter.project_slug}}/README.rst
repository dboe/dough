{% set is_open_source = cookiecutter.copyright_license != 'Unlicensed' -%}
library documentation
=====================

..
    [shields-start]
{%- if is_open_source %}
{%- if cookiecutter.pypi_username != "none" and cookiecutter.package_repository == "https://pypi.python.org/pypi/" %}
.. pypi
.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.distribution_name }}.svg
    :target: {{ cookiecutter.package_repository }}/{{ cookiecutter.distribution_name }}
{%- endif %}
{%- endif %}
{%- if cookiecutter.remote_provider != "none" %}
.. ci
.. image:: {{ get_remote_url() }}/badges/master/pipeline.svg
    :target: {{ get_remote_url() }}/-/pipelines/latest
.. latest release
.. image:: {{ get_remote_url() }}/-/badges/release.svg
    :target: {{ get_remote_url() }}/-/releases
.. coverage
.. image:: {{ get_remote_url() }}/badges/master/coverage.svg
    :target: {{ get_remote_url() }}/commits/master
.. docs
{%- if cookiecutter.remote_provider == "git.ipp-hgw.mpg.de" %}
.. hack (static "up") because IPP gitlab seems to not respond on GET. NOTE: Replace once, fixed
.. image:: https://img.shields.io/website?url=https://{{ get_remote_url(prefix="") }}&up_message=up&up_color=green&down_message=up&down_color=green&label=docs
{%- else %}
.. image:: https://img.shields.io/website-up-down-green-red/https/{{ get_doc_url(prefix="") }}.svg?label=docs
{%- endif %}
    :target: {{ get_doc_url() }}
{%- endif %}
.. pre-commit
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit
   :alt: pre-commit
..
    [shields-end]

Overview
--------

..
    [overview-start]

{{ cookiecutter.summary }}

..
    [overview-end]

{% if is_open_source -%}
Licensed under the ``{{ cookiecutter.copyright_license }}``
{%- endif %}

Resources
~~~~~~~~~

..
    [resources-start]

{% if get_doc_url() != "none" -%}
- Documentation: {{ get_doc_url() }}
{% endif -%}
{% if cookiecutter.remote_provider != "none" -%}
- Source code: {{ get_remote_url() }}
- Bug reports: {{ get_remote_url() }}/-/issues/
{% endif -%}
{% if cookiecutter.pypi_username != "none" -%}
{%- if cookiecutter.remote_provider == "git.ipp-hgw.mpg.de" %}
- Pypi: https://pypi.ipp-hgw.mpg.de/#browse/browse:pypi-hosted:{{ cookiecutter.distribution_name | replace("_", "-") }}
{%- else %}
- Pypi: https://pypi.python.org/pypi/{{ cookiecutter.distribution_name }}
{%- endif %}
{% endif %}

..
    [resources-end]

Features
~~~~~~~~

..
    [features-start]

The following features should be highlighted:

* TODO

..
    [features-end]

Installation
~~~~~~~~~~~~

..
    [install-start]
    
.. code-block:: shell

    pip install {{ cookiecutter.distribution_name }}

..
    [install-end]

Usage
~~~~~

..
    [usage-start]

To use '{{ cookiecutter.package_name }}' in a project use

.. code-block:: python

    import {{ cookiecutter.distribution_name }}

..
    [usage-end]
