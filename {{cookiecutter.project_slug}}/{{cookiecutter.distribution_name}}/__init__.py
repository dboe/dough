"""Top-level package of {{ cookiecutter.package_name }}."""

__author__ = """{{ cookiecutter.author }}"""
__email__ = "{{ cookiecutter.email }}"
__version__ = "{{ cookiecutter.package_version }}"
