============
Installation
============
..
    How to get it and set it up?
    [installation]

To install {{ cookiecutter.package_name }}, you have the following options:

.. tab-set::

    .. tab-item:: PyPi :octicon:`package`

        The preferred method to install {{ cookiecutter.package_name }} is to get the most recent stable release from PyPi:
        
        .. include:: ../../_dynamic/README.rst
            :start-after: [install-start]
            :end-before: [install-end]

        If you don't have `pip`_ installed, this `Python installation guide`_ can guide you through the process.
        
        .. _pip: https://pip.pypa.io
        .. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/

        .. dropdown:: Extras
            :icon: star
            
            Install a special extra:
                :code:`pip install {{ cookiecutter.distribution_name }}[extra]`

            All extras:
                :code:`pip install {{ cookiecutter.distribution_name }}[full]`

    .. tab-item:: Source :octicon:`code`

        First you have to retrieve the source code of {{ cookiecutter.package_name }}.
        You have the following options:
        
        .. tab-set::
        
            .. tab-item:: Git :octicon:`git-branch`

                To clone the public repository run

                .. code-block:: shell

                    git clone git://{{ get_remote_url(prefix="") }}

            .. tab-item:: Tarball :octicon:`gift`

                Either download the tarball `here <{{ get_remote_url() }}/tarball/master>`_ or run

                .. code-block:: shell

                    curl -OJL {{ get_remote_url() }}/tarball/master

                
        Once you have a copy of the source, navigate inside and install it with:

        .. code-block:: shell

            poetry install
{% if cookiecutter.data_science == "y" %}

.. dropdown:: Data version control (dvc)
    :icon: database

    To pull the version controlled data from a remote repo, do
    
    .. code-block:: shell

        dvc pull

{%- endif -%}