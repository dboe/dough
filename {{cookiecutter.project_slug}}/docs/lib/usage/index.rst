Usage
=====
..
    How to use? Give me a primer.
    [usage]

.. include:: ../../_dynamic/README.rst
   :start-after: [usage-start]
   :end-before: [usage-end]
{% if cookiecutter.data_science == "y" %}

Datascience Project Organization
--------------------------------

::
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting

Data version control
--------------------

TODO!!! dvc flag

TODO!!!
webdav: ask owner for .dvc/config.local file
put it into ./dvc

Run 

.. code-block:: shell

    dvc pull

{%- endif -%}