===
API
===

..
    How to dive deeper? Give me the api?
    [reference]
    
.. autosummary::
    :toctree: _autosummary
    :template: custom-module-template.rst
    :recursive:

    {{ cookiecutter.package_name }}
