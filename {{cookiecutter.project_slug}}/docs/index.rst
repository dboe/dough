.. div:: sd-text-center sd-text-primary sd-fs-1 sd-font-weight-bolder -sd-text-nowra
   
   {{ cookiecutter.package_name }}

.. div:: sd-sphinx-override sd-mb-3 sd-shadow-none sd-text-center sd-border-0 docutils no-underline secondary-background sd-card-body 

   .. include:: _dynamic/README.rst
      :start-after: [shields-start]
      :end-before: [shields-end]
     
.. grid:: 1 1 2 2
    :gutter: 1

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Overview
               :link: lib/overview/index
               :link-type: doc

               .. include:: _dynamic/README.rst
                  :start-after: [overview-start]
                  :end-before: [overview-end]

            .. grid-item-card:: Installation
               :link: lib/installation/index
               :link-type: doc

               .. include:: _dynamic/README.rst
                  :start-after: [install-start]
                  :end-before: [install-end]

    .. grid-item::

        .. grid:: 1 1 1 1
            :gutter: 1

            .. grid-item-card:: Usage
               :link: lib/usage/index
               :link-type: doc

               .. include:: _dynamic/README.rst
                  :start-after: [usage-start]
                  :end-before: [usage-end]

            .. grid-item-card:: API reference
               :link: api/_autosummary/{{ cookiecutter.distribution_name }}
               :link-type: doc
               :text-align: center

               :octicon:`codescan;5em;sd-text-info`

            .. grid-item-card:: API development
               :link: contributing/index
               :link-type: doc
               
               
               .. code-block:: shell
                  
                  pixi shell
                  pixi test
                  pixi publish

.. toctree::
   :hidden:

   lib/index
   contributing/index
   api/index
   about/index
