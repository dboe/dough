Contributor Guide
=================

Welcome to the community of {{ cookiecutter.package_name }}!

..
   [contributing]

.. highlight:: shell

Thank you for considering contributing to the project!
Your contributions are greatly appreciated, and every little bit helps to make the project better for everyone.

.. include:: contribution_types/index.rst
   :start-after: [intro-start]
   :end-before: [intro-end]

.. toctree::
   :maxdepth: 2
   
   contribution_types/index
   
.. include:: setup/index.rst
   :start-after: [intro-start]
   :end-before: [intro-end]

.. toctree::
   :maxdepth: 2
   
   setup/index

.. include:: workflow/index.rst
   :start-after: [intro-start]
   :end-before: [intro-end]

.. toctree::
   :maxdepth: 2
   
   workflow/index
 
.. include:: guidelines/index.rst
   :start-after: [intro-start]
   :end-before: [intro-end]

.. toctree::
   :maxdepth: 2
   
   guidelines/index
