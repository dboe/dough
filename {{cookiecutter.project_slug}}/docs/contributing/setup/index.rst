..
   [intro-start]

Setup and Install
-----------------

Ready to contribute?
Here's how to set up `{{ cookiecutter.package_name }}` for local development.

..
   [intro-end]

1. If you are part of the project team, you are welcome to submit changes through your own development branch in the main repository.
   However, if you are not part of the team, we recommend `forking <{{ get_remote_url() }}/-/forks/new/>`_ the project and submitting changes through a pull request from your forked repository.
   This allows us to review and discuss your changes before merging them into the main project.
2. Clone the repository to your local machine::

    .. code-block:: shell

        git clone git@{{ cookiecutter.remote_provider }}:{{ cookiecutter.remote_namespace }}/{{ cookiecutter.project_slug }}.git

    Change adequately if you forked.

3. Set up and activate your local development environment::

    .. code-block:: shell
        
        cd {{ cookiecutter.project_slug }}/
        pixi shell
    
    The first time your run `pixi shell`, pixi will setup a virtual environment for you, install all packages there and install `pre-commit <https://pre-commit.com/>`_ so that can be slow.
    After that, it will activate the environment such that from now on your binaries are available within this virtual environment.
    
You are now ready to go.
Have a look at 
    
    .. code-block:: shell

        pixi task list

to get an overview over the daily operations that are automatized for you.
