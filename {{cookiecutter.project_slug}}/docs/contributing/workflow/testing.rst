Testing
-------

To run tests, use

    .. code-block:: shell

        pixi run test

To run a subset of tests, you have the following options:

- You can run a subset of tests by specifying a test file or test name

    .. code-block:: shell

        pytest tests/test_package.py

        pytest tests/test_package.py::Test_{{ cookiecutter.distribution_name }}::test_version_type
    
- To test doctests, in docstrings or documentation, use the flag `--doctest-modules` (active by default via pyproject.toml)

    .. code-block:: shell

        pytest --doctest-modules docs/usage.rst

        pytest --doctest-modules {{ cookiecutter.distribution_name }}/__init__.py -k "MyClass.function_with_doctest"

- Use the `--trace` option to directly jump into a pdb debugger on fails.

To run tests with code coverage, use the command

    .. code-block:: shell

        pixi run coverage

This will generate an HTML report showing the coverage of each file in the project.

You will want to skip tests. For this purpose, you can use the following `marks` depending on the circumstances:

- Expensive/'slow' tests should not be run all the time. Mark them as slow via 

    .. code-block:: python

        @pytest.mark.slow
        def test_expensive_thing():
            import time

            time.sleep(100)

  and use the `--slow` command line flag to activate them

    .. code-block:: shell
    
        pytest --slow
        
- Some tests concern plotting and require visual assertion via. To activate a mock of `matplotlib.pyplot.show`, decorate

    .. code-block:: python

        @pytest.mark.plot
        def test_visual_assert_required():
            import matplotlib.pyplot as plt

            plt.figure()
            plt.show()

    and use the `--plot` command line flag to disable the mock and show the figures

    .. code-block:: shell
    
        pytest --plot

- If e.g. a database is required that you don't have access to in continuous integration, use the following mark to skip it on ci automatically
    .. code-block:: python

        @pytest.mark.skipif(os.environ.get("CI_RUNNING") == "true")
        def test_that_requires_db():
            db.query(thing)
