Daily Operations
----------------

- Work on the virtual environment by activating it::

    .. code-block:: shell

        pixi shell
        
    The first time you run `pixi shell` the environment will be created so this can take some time.

- Work on a branch different from `master/main` for local development::

    .. code-block:: shell

        git checkout -b <name-of-your-bugfix-or-feature>

   Now you can make your changes locally.

- When you're done making changes, check that these pass the linters and tests::

    .. code-block:: shell

        pixi run test
    
- Commit your changes and push your branch to origin::

    .. code-block:: shell

        git add <files to be added to your commit>
        git commit -m "Your detailed description of your changes."
        git push origin name-of-your-bugfix-or-feature

- Submit a `merge request <{{ get_remote_url() }}/-/merge_requests/>`_ through the repository website Follow the `Guidelines <#guidelines>` below.
