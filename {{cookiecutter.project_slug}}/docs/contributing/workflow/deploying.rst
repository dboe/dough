Deploying
---------

A reminder for the maintainers on how to deploy.
Make sure all your changes are committed.
Then run

    .. code-block:: shell

        pixi run publish

.. dropdown:: Failed CI deploy stage
    :icon: alert
    
    In case of failure in the CI deploy stage after :code:`make publish`, use the following rule to delete the tag (also remote)

    .. code-block:: shell
    
        pixi run untag
    
If something fails on the ci side, you can make use of 

{% if cookiecutter.pypi_username != "none" -%}
The CI will then deploy to PyPI if tests pass.

{%- endif %}
