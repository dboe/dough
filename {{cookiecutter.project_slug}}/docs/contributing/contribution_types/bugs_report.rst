Report Bugs
~~~~~~~~~~~

If you encounter a bug in the project, please report it by `creating an issue on the project's issue tracker <{{ get_remote_url() }}/-/issues/>`_.
When reporting a bug, please include:

- Your operating system name and version
- Any details about your local setup that might be helpful in troubleshooting
- Detailed steps to reproduce the bug

If you want quick feedback, it is helpful to mention specific developers
(@developer_name) or @all. This will trigger a mail to the corresponding developer(s).
