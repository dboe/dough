Implementing Features
~~~~~~~~~~~~~~~~~~~~~

If you would like to help implement a new feature in the project, take a look at the issues tagged as "enhancement" and "help wanted" in the project's issue tracker.
These issues are open to anyone who wants to help implement them.
