Writing Tests
~~~~~~~~~~~~~

`{{ cookiecutter.package_name }}` profits a lot from better :ref name="Testing":`testing`.
We encourage you to add unittests with the `pytest` module (`unittest` module is OK, too) in the `tests` directory or doctests (as part of docstrings or in the documentation).
