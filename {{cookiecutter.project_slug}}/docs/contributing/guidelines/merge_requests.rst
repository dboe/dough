Merge Request and Code Review Guidelines
----------------------------------------

All code contributions to the project are reviewed by project maintainers before they are merged.
To ensure that your code contribution is accepted, please follow these guidelines:

Before you submit a `merge request <{{ get_remote_url() }}/-/merge_requests/>`_, check that you follow these guidelines:

1. Write code that is well-documented and easy to understand.
2. Write tests that verify the behavior of your code.
    The merge request should work for all supported python versions and operation systems.
3. Use the project's coding style guidelines.
4. Submit a pull request that describes your changes and why they are valuable to the project.
5. Respond to feedback from code reviewers in a timely manner.
   and make sure that the tests pass for all supported Python versions.
