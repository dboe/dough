{% if cookiecutter.remote_provider == "git.ipp-hgw.mpg.de" -%}
image: nexus-apt.ipp-hgw.mpg.de:18095/python:3.10.10
{% else -%}
image: python:3.10.10
{% endif -%}

variables:
  # Change pip's cache directory to be inside the project directory since we can
  # only cache local items.
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  # If you do not have access to e.g. databases in the ci, use pytest.mark.skipif to skip a test:
  #   @pytest.mark.skipif(os.environ.get('CI_RUNNING') == 'true')
  CI_RUNNING: "true"

# Pip's cache doesn't store the python packages
# https://pip.pypa.io/en/stable/reference/pip_install/#caching
#
# If you want to also cache the installed packages, you have to install
# them in a virtualenv and cache it as well.
cache:
  key: one-key-to-rule-them-all
  paths:
    - .cache/pip
    - .cache/apt
    - .cache/virtualenvs

stages:
  - build
  - test
  - deploy

#commands to run in the Docker container before starting each job
before_script:
  - pip install poetry
  - poetry config cache-dir $(pwd)/.cache
  - mkdir -p .cache/apt
  - apt-get update -yqq
  - apt-get -o dir::cache::archives=".cache/apt" install -y -qq gfortran libopenblas-dev liblapack-dev
  - apt-get -o dir::cache::archives=".cache/apt" install -y -qq libgmp-dev
  - poetry install --with docs # --extras extra we require
  - source $(poetry env info -p)/bin/activate

dist:
  stage: build
  script:
    - poetry build
  artifacts:
    paths:
      - dist/*.whl
    expire_in: 1h
  only:
    - tags

pages:
  stage: build
  script:
    - sphinx-build -M html docs docs/_build/ -E -a -j auto --keep-going
    - mkdir -p public
    - rm -rf public/*
    - mv docs/_build/html/* public/  # add it to pages. Pages is exposing public/index.html
  only:
    - master
    - tags
  cache:
    paths:
      - public
  artifacts:
    paths:
      - public
      - docs

lint:
  stage: test
  script:
    - ruff check .

test:
  stage: test
  script:
    - coverage run -m pytest
    - coverage report
    - coverage xml
    - mkdir -p report
    - rm -rf report/*
    - mv coverage.xml report/
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: report/coverage.xml

{% if cookiecutter.pypi_username != "none" -%}
pypi:
  stage: deploy
  script:
{%- if cookiecutter.remote_provider == "git.ipp-hgw.mpg.de" %}
    - poetry config repositories.ipp-pypi https://pypi.ipp-hgw.mpg.de/repository/pypi-hosted/
    - poetry publish --repository ipp-pypi -u $PYPI_USERNAME -p $PYPI_PASSWORD
{%- else %}
    - poetry publish -u $PYPI_USERNAME -p $PYPI_PASSWORD
{%- endif %}
  rules:
    # for debugging: git commit -am "deb" && git push && bumpversion patch && git tag -l --sort=-v:refname | head -n 1 | git push origin
    - if: $CI_COMMIT_TAG =~ /^v[0-9]+\.[0-9]+\.[0-9]+$/
{%- endif %}
