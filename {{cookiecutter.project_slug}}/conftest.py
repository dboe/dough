import logging
import unittest.mock

import pytest

try:
    import matplotlib
except ModuleNotFoundError:
    matplotlib = None
try:
    import numpy as np
except ModuleNotFoundError:
    np = None


# Add auto-imports for doctests
if np is not None:

    @pytest.fixture(autouse=True)
    def add_np(doctest_namespace):
        doctest_namespace["np"] = np


# Patching plotting
@pytest.fixture(scope="session", autouse=True)
def default_session_fixture(request):
    """
    :type request: _pytest.python.SubRequest
    :return:
    """
    log = logging.getLogger()
    log.info("Patching `show`")
    patch = unittest.mock.patch("matplotlib.pyplot.show")
    if matplotlib is not None:
        if not request.config.getoption("--plot"):
            patch.start()


def pytest_addoption(parser):
    if matplotlib is not None:
        parser.addoption("--plot", action="store_true", default=False, help="Activate plot tests")
    parser.addoption("--slow", action="store_true", default=False, help="Activate slow tests")


def pytest_configure(config):
    if matplotlib is not None:
        config.addinivalue_line("markers", "plot: mark test as requiring visual assertion by human")
    config.addinivalue_line("markers", "slow: mark test as slow")


def pytest_collection_modifyitems(config, items):
    skip_slow = pytest.mark.skip(reason="need --slow option to run")
    for item in items:
        if "slow" in item.keywords and not config.getoption("--slow"):
            item.add_marker(skip_slow)
