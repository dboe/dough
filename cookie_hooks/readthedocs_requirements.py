import os
import subprocess

try:
    import tomllib
except ModuleNotFoundError:
    import tomli as tomllib


def main() -> int:
    path = "docs/requirements.txt"
    head = """
# Autogenerated by Makefile from pyproject.toml docs dependency group section. Remove this line if you want to fix this file.
"""

    status = 0

    if not os.path.exists("pyproject.toml"):
        print("File pyproject.toml not found.")
        return status

    if os.path.exists(path):
        with open(path, "r") as f:
            line = f.readline()
            if line.startswith(head[:20]):
                print("Updating " + path)
            else:
                print("User defined requirements already existing.")
                return status
    with open("pyproject.toml", "rb") as f:
        config = tomllib.load(f)
    deps = config["tool.poetry.group.docs.dependencies"]
    deps = [x for x in deps if x]

    with open(path, "w") as f:
        f.write("\n".join([head] + deps) + "\n")

    subprocess.call(["git", "add", path])

    return status


if __name__ == "__main__":
    status = main()
    print(status)
