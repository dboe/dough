import sys 
import pathlib


def test_bake_project(cookies):
    sys.path.append(str(pathlib.Path(__file__).parent.parent))  # this needs to find the local_extension folder
    result = cookies.bake(extra_context={"repo_name": "helloworld"})

    assert result.exit_code == 0
    assert result.exception is None

    assert result.project_path.name == "helloworld"
    assert result.project_path.is_dir()
    
    # pre-commit install
    # pre-commit run --all-files
    # git status .
